// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/BoxComponent.h"
#include "RoomSpawner.generated.h"

class UBoxComponent;
class ARoomCollection;
class ARoom;

UENUM(BlueprintType)
enum class Direction : uint8
{
	Top, Right, Bottom, Left, None
};

UCLASS()
class COSMICSKAZKI_API ARoomSpawner : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ARoomSpawner();

	UPROPERTY()
		UBoxComponent* BoxComponent; 
	UPROPERTY(BlueprintReadWrite)
		bool bIsRoomSpawned = false;
	UPROPERTY(BlueprintReadWrite)
		Direction thisDirection;
	UPROPERTY()
		TSubclassOf<ARoom> RoomClass;
		
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	
	UFUNCTION()
		void SpawnRoom();
	UFUNCTION()
		void OnActorBeginOverlapHandler(UPrimitiveComponent* OverlappedComponent,
			AActor* OtherActor,
			UPrimitiveComponent* OtherComp,
			int32 OtherBodyIndex,
			bool bFromSweep,
			const FHitResult& SweepResult);
};
