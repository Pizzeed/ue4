// Fill out your copyright notice in the Description page of Project Settings.


#include "RoomSpawner.h"
#include "RoomCollection.h"
#include "Room.h"
#include "CosmicSkazkiGameModeBase.h"

// Sets default values
ARoomSpawner::ARoomSpawner()
{
	BoxComponent = CreateDefaultSubobject<UBoxComponent>(TEXT("BoxCollision"));
	RootComponent = BoxComponent;
	BoxComponent->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	BoxComponent->SetCollisionResponseToAllChannels(ECR_Overlap);
	BoxComponent->OnComponentBeginOverlap.AddDynamic(this, &ARoomSpawner::OnActorBeginOverlapHandler);
}

// Called when the game starts or when spawned
void ARoomSpawner::BeginPlay()
{
	Super::BeginPlay();
	if (!bIsRoomSpawned)
	{
		FTimerHandle UnusedHandle;
		GetWorldTimerManager().SetTimer(UnusedHandle, this, &ARoomSpawner::SpawnRoom, 0.2f, false);
	}
}

void ARoomSpawner::SpawnRoom()
{
	TArray<TSubclassOf<ARoom>> chosenRooms;
	ACosmicSkazkiGameModeBase* currentGM = Cast<ACosmicSkazkiGameModeBase>(GetWorld()->GetAuthGameMode());
	if (IsValid(currentGM))
	{
		switch (thisDirection)
		{
			case Direction::Top:
			{
				if (currentGM->currentRooms < currentGM->maxRooms)
				{
					chosenRooms = currentGM->currentCollection->BottomDoorRooms;
				}
				else
				{
					chosenRooms = currentGM->currentCollection->BottomDoorDeadEndRooms;
				}
			}
			break;
			case Direction::Right:
			{
				if (currentGM->currentRooms < currentGM->maxRooms)
				{
					chosenRooms = currentGM->currentCollection->LeftDoorRooms;
				}
				else
				{
					chosenRooms = currentGM->currentCollection->LeftDoorDeadEndRooms;
				}
			}
			break;
			case Direction::Bottom:
			{
				if (currentGM->currentRooms < currentGM->maxRooms)
				{
					chosenRooms = currentGM->currentCollection->TopDoorRooms;
				}
				else
				{
					chosenRooms = currentGM->currentCollection->TopDoorDeadEndRooms;
				}
			}
			break;
			case Direction::Left:
			{
				if (currentGM->currentRooms < currentGM->maxRooms)
				{
					chosenRooms = currentGM->currentCollection->RightDoorRooms;
				}
				else
				{
					chosenRooms = currentGM->currentCollection->RightDoorDeadEndRooms;
				}
			}
			break;
		}
	}

	int rnd = FMath::RandRange(0, chosenRooms.Num() - 1);
	RoomClass = chosenRooms[rnd];
	GetWorld()->SpawnActor<ARoom>(RoomClass, FTransform(this->GetActorLocation()));
	bIsRoomSpawned = true;
	currentGM->currentRooms++;
}

void ARoomSpawner::OnActorBeginOverlapHandler(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (IsValid(Cast<ARoomSpawner>(OtherActor)) && !bIsRoomSpawned)
	{
		if (Cast<ARoomSpawner>(OtherActor)->bIsRoomSpawned)
		{
			this->Destroy();
		}
		else if (thisDirection > Cast<ARoomSpawner>(OtherActor)->thisDirection)
		{
			this->Destroy();
		}
	}
}

