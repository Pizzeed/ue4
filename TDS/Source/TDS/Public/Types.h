#pragma once

#include "CoreMinimal.h"
#include "Styling/SlateBrush.h"
#include "StatusEffect.h"
#include "Types.generated.h"


UENUM(BlueprintType)
enum class EStatusEffectType : uint8
{
	Bleed,
	Poison,
	DamageDebuff,
	Slowdown,
	Stun
};

UENUM(BlueprintType)
enum class EQuality : uint8
{
	Common,
	Uncommon,
	Rare,
	VeryRare,
	Epic,
	Legendary,
	Godly
};

UENUM(BlueprintType)
enum class EAmmoType : uint8
{
	Light, Heavy, Artillery, Energy, None
};

UENUM(BlueprintType)
enum class EPickupType : uint8
{
	Ammo, Usable, Weapon
};

USTRUCT(BlueprintType)
struct TDS_API FInventoryItem
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		UTexture2D* ItemImage = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FName ItemName;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		EQuality ItemQuality;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FText ItemDescription;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		bool bStackable;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (EditCondition = "bStackable", EditConditionHides))
		int Quantity;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (EditCondition = "bStackable", EditConditionHides))
		int Stack;
};

USTRUCT(BlueprintType)
struct TDS_API FAmmo : public FInventoryItem
{
	GENERATED_BODY()

		FAmmo() {Type = EAmmoType::None; BaseDamage = 0;}
	
		FAmmo(EAmmoType type, int dmg, TArray<TSubclassOf<AStatusEffect>> effects) : Type(type), BaseDamage(dmg), AdditionalEffects(effects) {};

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		EAmmoType Type;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int BaseDamage;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		TArray<TSubclassOf<AStatusEffect>> AdditionalEffects;

	//static const FAmmo None;
};

inline bool operator==(const FAmmo a1, const FAmmo a2)
{
	return
		a1.AdditionalEffects == a2.AdditionalEffects &&
		a1.BaseDamage == a2.BaseDamage &&
		a1.ItemQuality == a2.ItemQuality &&
		a1.Type == a2.Type &&
		a1.bStackable == a2.bStackable;
}




