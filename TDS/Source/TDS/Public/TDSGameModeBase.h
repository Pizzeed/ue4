#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "RoomCollection.h"
#include "TDSGameModeBase.generated.h"

UCLASS()
class TDS_API ATDSGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
public:
	UPROPERTY()
	int CurrentRooms;
	UPROPERTY(EditAnywhere)
	int MaxRooms;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FRoomCollection CurrentCollection;
};
