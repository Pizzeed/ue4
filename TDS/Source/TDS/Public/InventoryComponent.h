#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "PickupItem.h"
#include "InventoryComponent.generated.h"

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class TDS_API UInventoryComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	UInventoryComponent();

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		uint8 WeaponSlots;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		uint8 AmmoSlots;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		uint8 UsableItemSlots;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
		uint8 EquippedWeapon;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TArray<FWeaponInventoryItem> Weapons;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TArray<FLoadedAmmo> AmmoOwned;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TArray<FUsableItem> UsableItems;


	UFUNCTION(BlueprintPure, meta = (DisplayName = "Equal(FUsableItem)", CompactNodeTitle = "==", Keywords = "== equal"), Category = "Custom|FUsableItem")
		static bool EqualEqual_FUsableItemFUsableItem(FUsableItem A, const FUsableItem B);

protected:
	virtual void BeginPlay() override;

public:	
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;


	UFUNCTION(BlueprintPure, meta = (DisplayName = "Get Ammo Amount Of Type", Keywords = "get ammo all amount type"))
		int GetAmmoAmountOfType(EAmmoType type);

	UFUNCTION(BlueprintPure, meta = (DisplayName = "Get Ammo Amount Of Type Array", Keywords = "get ammo all amount type"))
		int GetAmmoAmountOfTypeArr(TArray<EAmmoType> type);
};
