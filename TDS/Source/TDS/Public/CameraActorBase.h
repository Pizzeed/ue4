// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "CameraActorBase.generated.h"

class UCameraComponent;
class USpringArmComponent;

UCLASS()
class TDS_API ACameraActorBase : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ACameraActorBase();

	UPROPERTY()
	UCameraComponent* CamComponent;
	UPROPERTY()
	USpringArmComponent* ArmComponent;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;


};
