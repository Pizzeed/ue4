#pragma once

#include "CoreMinimal.h"
#include "WeaponBase.h"
#include "MeleeWeapon.generated.h"

UCLASS()
class TDS_API AMeleeWeapon : public AWeaponBase
{
	GENERATED_BODY()
	
public:
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Melee")
		float AttackSpeed;
	
protected:

	virtual void BeginPlay() override;

	virtual void Tick(float DeltaTime) override;
};
