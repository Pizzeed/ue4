#pragma once

#include "CoreMinimal.h"
#include "WeaponBase.h"
#include "RangedWeapon.generated.h"

UENUM(BlueprintType)
enum class EMultishotType : uint8
{
	Radial,
	Straight
};

UENUM(BlueprintType)
enum class ERangedWeaponType : uint8
{
	Hitscan, Projectile
};

UCLASS()
class TDS_API ARangedWeapon : public AWeaponBase
{
	GENERATED_BODY()
	
public:

	ARangedWeapon();

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Components)
		class UArrowComponent* Arrow;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Components)
		USceneComponent* GunPoint;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Stats")
		float RateOfFire;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Stats")
		float RangeOfFire;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Stats")
		float BulletDamageMultiplier;

	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly, Category = "Ammo")
		TArray<EAmmoType> AllowedAmmo;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Ammo")
		int MaxAmmo;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Ammo")
		FLoadedAmmo AmmoIn;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = "Stats")
		ERangedWeaponType ShotType;

	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly, Category = "Stats|Projectile", meta = (EditCondition = "Type == ShotType::Projectile"))
		float ProjectileStartSpeed;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = "Stats|Projectile", meta = (EditCondition = "Type == ShotType::Projectile"))
		bool ProjectileBallistic;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Stats|Projectile", meta = (EditCondition = "Type == ShotType::Projectile"))
		TSubclassOf <class AProjectileBase> ProjectileClass;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Stats|Multishot")
		bool IsMultishot;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Stats|Multishot", meta = (EditCondition = "IsMultishot"))
		EMultishotType MultishotType;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Stats|Multishot", meta = (EditCondition = "IsMultishot"))
		int ShotCount;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Stats|Multishot", meta = (EditCondition = "IsMultishot && MultishotType == EMultishotType::Radial"))
		float MultishotRadialAngleDeg;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Stats|Multishot", meta = (EditCondition = "IsMultishot && MultishotType == EMultishotType::Straight"))
		float MultishotStraightOffset;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Stats|Multishot", meta = (EditCondition = "IsMultishot"))
		bool MultishotUseAmmoForEachShot;


protected:

	virtual void BeginPlay() override;

	virtual void Tick(float DeltaTime) override;

public:

	UFUNCTION(BlueprintCallable)
		void Reload();
	
	UFUNCTION()
		void Shoot(int amount);

	virtual void Attack() override;

	virtual bool CheckAttackAvailable() override;

};
