#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Types.h"
#include "HealthComponent.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_ThreeParams(FDamageReceivedDelegate, AActor*, Receiver, AActor*, Implier, int, Amount);

DECLARE_DYNAMIC_MULTICAST_DELEGATE_ThreeParams(FHealthGainedDelegate, AActor*, Receiver, AActor*, Implier, int, Amount);

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FDeathDelegate, AActor*, Dead, AActor*, Killer);

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class TDS_API UHealthComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	UHealthComponent();

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
		TArray<AStatusEffect*> AppliedEffects;

protected:

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		int MaxHealth = 100;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		int CurrentHealth = 100;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		int Armor = 0;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		int DamageCoefficient = 1;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		int HealCoefficient = 1;

	UPROPERTY(BlueprintAssignable)
		FDamageReceivedDelegate OnDamageTaken;
		
	UPROPERTY(BlueprintAssignable)
		FHealthGainedDelegate OnHealthGained;

	UPROPERTY(BlueprintAssignable)
		FDeathDelegate OnDead;

	virtual void BeginPlay() override;

public:	
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	UFUNCTION(BlueprintCallable)
		void Damage(int Amount, AActor* sender);

	UFUNCTION(BlueprintCallable)
		void Die(AActor* Killer);

	UFUNCTION(BlueprintCallable)
		void Heal(int Amount, AActor* sender);
};
