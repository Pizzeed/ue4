#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Types.h"
#include "PlayerBase.generated.h"

class ACameraActorBase;
class UInventoryComponent;
class UHealthComponent;

UCLASS()
class TDS_API APlayerBase : public ACharacter
{
	GENERATED_BODY()

public:
	APlayerBase();


	UPROPERTY(EditAnywhere)
		ACameraActorBase* Cam;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Components)
		UInventoryComponent* Inventory;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Components)
		UHealthComponent* HealthComponent;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		float MaxStamina;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		float DefaultMovementSpeed = 3;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float RunModifier = 2.5;

	UPROPERTY(EditAnywhere)
		float MaxCameraDistance = 100;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		class AWeaponBase* CurrentWeapon;

	UPROPERTY(BlueprintReadWrite)
		float CurrentStamina;

	UPROPERTY(BlueprintReadWrite)
		float CurrentMovementSpeed = 3;

	UPROPERTY(BlueprintReadOnly)
		FVector MovementVector;

	UPROPERTY(BlueprintReadOnly)
		bool bIsSprinting = false;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
		bool bIsAttacking = false;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
		bool bIsReloading = false;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Other")
		FName weaponSocketName;

protected:
	virtual void BeginPlay() override;

	

public:	
	virtual void Tick(float DeltaTime) override;

	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	bool HasAmmo(FAmmo type);

	bool HasAmmo(EAmmoType type);

	bool HasAmmo(TArray<EAmmoType> types);

	FAmmo FindAmmo(TArray<EAmmoType> types);
	
	FAmmo FindAmmo(EAmmoType type);


	UFUNCTION()
		void HorizontalInput(float value);

	UFUNCTION()
		void VerticalInput(float value);

	UFUNCTION()
		void CameraScale(float value);

	UFUNCTION()
		void SwitchWeaponHandler(float value);

	UFUNCTION()
		void ReloadStart();

	UFUNCTION(BlueprintCallable)
		void ReloadFinish();

	UFUNCTION()
		void OnPickUp();

	UFUNCTION(BlueprintCallable)
		void SwitchAmmo(FAmmo switchTo);

	UFUNCTION(BlueprintImplementableEvent)
		void UpdateInventory();
};
