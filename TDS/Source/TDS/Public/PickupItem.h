#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Types.h"
#include "WeaponBase.h"
#include "PickupItem.generated.h"

USTRUCT(BlueprintType)
struct TDS_API FUsableItem : public FInventoryItem
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TSubclassOf<class APickupItem> ItemClass;
};

inline bool operator== (const FUsableItem one, const FUsableItem other)
{
	return
		one.Stack == other.Stack &&
		one.bStackable == other.bStackable &&
		one.ItemQuality == other.ItemQuality &&
		one.ItemName == other.ItemName;
}


UCLASS()
class TDS_API APickupItem : public AActor
{
	GENERATED_BODY()
	
public:	
	APickupItem();

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		EPickupType PickupType;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (EditCondition="PickupType==EPickupType::Ammo", EditConditionHides))
		FAmmo AmmoInfo;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (EditCondition = "PickupType==EPickupType::Usable", EditConditionHides))
		FUsableItem UsableItemInfo;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (EditCondition = "PickupType==EPickupType::Weapon", EditConditionHides))
		FWeaponInventoryItem WeaponInfo;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		bool bRotate = true;

protected:
	virtual void BeginPlay() override;

public:	
	virtual void Tick(float DeltaTime) override;



	UFUNCTION(BlueprintCallable)
		virtual void UseItem();
	UFUNCTION()
		virtual void PickUp();
};
