#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Room.h"
#include "RoomCollection.generated.h"

USTRUCT(BlueprintType)
struct TDS_API FRoomCollection
{
	GENERATED_BODY()
public:
	UPROPERTY(EditAnywhere)
		TArray<TSubclassOf<ARoom>> TopDoorRooms;
	UPROPERTY(EditAnywhere)
		TArray<TSubclassOf<ARoom>> BottomDoorRooms;
	UPROPERTY(EditAnywhere)
		TArray<TSubclassOf<ARoom>> LeftDoorRooms;
	UPROPERTY(EditAnywhere)
		TArray<TSubclassOf<ARoom>> RightDoorRooms;
	UPROPERTY(EditAnywhere)
		TArray<TSubclassOf<ARoom>> TopDoorDeadEndRooms;
	UPROPERTY(EditAnywhere)
		TArray<TSubclassOf<ARoom>> BottomDoorDeadEndRooms;
	UPROPERTY(EditAnywhere)
		TArray<TSubclassOf<ARoom>> LeftDoorDeadEndRooms;
	UPROPERTY(EditAnywhere)
		TArray<TSubclassOf<ARoom>> RightDoorDeadEndRooms;
};
