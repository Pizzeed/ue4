#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "WeaponBase.h"
#include "ProjectileBase.generated.h"

UCLASS()
class TDS_API AProjectileBase : public AActor
{
	GENERATED_BODY()

public:
	AProjectileBase();

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Components)
		class USphereComponent* CollisionSphere = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Components)
		class UProjectileMovementComponent* ProjectileMovement = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Components)
		class UStaticMeshComponent* Mesh;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Components)
		class UParticleSystemComponent* ProjectileFX;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FAmmo AmmoInfo;

protected:
	virtual void BeginPlay() override;

public:
	virtual void Tick(float DeltaTime) override;
	UFUNCTION()
		void CollisionSphereHit(
			class UPrimitiveComponent* HitComp,
			AActor* OtherActor,
			UPrimitiveComponent* OtherComp,
			FVector NormalImpulse,
			const FHitResult& Hit);
	UFUNCTION()
		void CollisionSphereBeginOverlap(
			class UPrimitiveComponent* HitComp,
			AActor* OtherActor,
			UPrimitiveComponent* OtherComp,
			int32 OtherBodyIndex,
			bool bFromSweep,
			const FHitResult& Hit);
	UFUNCTION()
		void CollisionSphereEndOverlap(
			class UPrimitiveComponent* HitComp,
			AActor* OtherActor,
			UPrimitiveComponent* OtherComp,
			int32 OtherBodyIndex);
};