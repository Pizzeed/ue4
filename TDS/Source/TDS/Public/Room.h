#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Room.generated.h"

class ARoomSpawner;
class UStaticMeshComponent;
class USpotLightComponent;

UCLASS()
class TDS_API ARoom : public AActor
{
	GENERATED_BODY()
	
public:	
	ARoom(const FObjectInitializer& OI);

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	float RoomSpawnerOffset;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	UStaticMeshComponent* MainFloorPlatform;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	USpotLightComponent* LightSource;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	bool IsTopDoor;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	bool IsBottomDoor;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	bool IsLeftDoor;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	bool IsRightDoor;
	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<ARoomSpawner> RoomSpawnerClass;

protected:
	virtual void BeginPlay() override;
public:	
	virtual void Tick(float DeltaTime) override;

};
