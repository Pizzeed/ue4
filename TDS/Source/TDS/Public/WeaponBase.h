#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Types.h"
#include "StatusEffect.h"
#include "WeaponBase.generated.h"

class AProjectileBase;

UENUM(BlueprintType)
enum class EWeaponType : uint8
{
	Melee,
	Ranged
};

USTRUCT(BlueprintType)
struct TDS_API FLoadedAmmo
{
	GENERATED_BODY()

	FLoadedAmmo() {Info = FAmmo();}
	
	FLoadedAmmo(FAmmo info) : Info(info) {};

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FAmmo Info;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int Amount = 0;

	//static const FLoadedAmmo None;
};

USTRUCT(BlueprintType)
struct TDS_API FWeaponInventoryItem : public FInventoryItem
{
	GENERATED_BODY()

		FWeaponInventoryItem();
	
		FWeaponInventoryItem(TSubclassOf<AWeaponBase> weaponClass): WeaponClass(weaponClass) {};
	
		FWeaponInventoryItem(TSubclassOf<AWeaponBase> weaponClass, FLoadedAmmo ammoIn/* = FLoadedAmmo::None*/) : WeaponClass(weaponClass), LoadedAmmo(ammoIn) {};

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "General")
		TSubclassOf<AWeaponBase> WeaponClass;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "General")
		FLoadedAmmo LoadedAmmo;

};


UCLASS()
class TDS_API AWeaponBase : public AActor
{
	GENERATED_BODY()
	
public:	
	AWeaponBase();

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Components)
		class UStaticMeshComponent* Mesh;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Components)
		class USkeletalMeshComponent* SkeletalMesh;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Components)
		class UParticleSystemComponent* WeaponFX = nullptr;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Components)
		class UAudioComponent* Audio = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		class APlayerBase* OwnerChar;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Stats")
		TArray<TSubclassOf<AStatusEffect>> AdditionalEffects;

	UPROPERTY(EditAnyWhere, BlueprintReadWrite)
	EWeaponType Type;

	UPROPERTY(BlueprintReadWrite)
		bool IsAttacking;

	UPROPERTY(BlueprintReadWrite)
		float AttackCooldown = 0.0f;

protected:
	virtual void BeginPlay() override;

	virtual void Tick(float DeltaTime) override;

public:	

	UFUNCTION()
		virtual void Attack();

	UFUNCTION()
		virtual bool CheckAttackAvailable();

};

inline bool operator==(const FLoadedAmmo a1, const FLoadedAmmo a2)
{
	return a1.Info == a2.Info;
}