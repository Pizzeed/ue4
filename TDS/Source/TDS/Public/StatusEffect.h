#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "StatusEffect.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FStatusEffectAppliedDelegate, AStatusEffect*, Effect, AActor*, ImpliedTo);

UCLASS()
class TDS_API AStatusEffect : public AActor
{
	GENERATED_BODY()

public:

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	class UParticleSystemComponent* EffectFX;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	class USoundCue* EffectAudioCue;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	uint8 Level;

	UPROPERTY(BlueprintReadWrite)
	AActor* ImpliedTo;

	UPROPERTY(BlueprintReadWrite)
	AActor* Implier;

	UPROPERTY(BlueprintAssignable)
	FStatusEffectAppliedDelegate OnStatusEffectApplied;

	UFUNCTION(BLueprintCallable)
		void ApplyEffect();

protected:
	virtual void BeginPlay() override;

public:
	virtual void Tick(float DeltaTime) override;

};
