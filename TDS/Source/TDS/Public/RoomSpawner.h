#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/BoxComponent.h"
#include "RoomSpawner.generated.h"

class UBoxComponent;
class ARoom;

UENUM(BlueprintType)
enum class Direction : uint8
{
	Top, Right, Bottom, Left, None
};

UCLASS()
class TDS_API ARoomSpawner : public AActor
{
	GENERATED_BODY()
	
public:	
	ARoomSpawner();

	UPROPERTY()
		UBoxComponent* BoxComponent; 
	UPROPERTY(BlueprintReadWrite)
		bool bIsRoomSpawned = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		Direction thisDirection;
	UPROPERTY(EditAnywhere)
		bool IsStarter;
	UPROPERTY()
		TSubclassOf<ARoom> RoomClass;
	UPROPERTY(EditAnywhere)
		TSubclassOf<ARoom> StarterRoomClass;
		
protected:
	virtual void BeginPlay() override;

public:	
	
	UFUNCTION()
		void SpawnRoom();
	UFUNCTION()
		void OnActorBeginOverlapHandler(UPrimitiveComponent* OverlappedComponent,
			AActor* OtherActor,
			UPrimitiveComponent* OtherComp,
			int32 OtherBodyIndex,
			bool bFromSweep,
			const FHitResult& SweepResult);
};
