// Fill out your copyright notice in the Description page of Project Settings.


#include "CameraActorBase.h"
#include "Camera/CameraComponent.h"
#include "GameFramework/SpringArmComponent.h"

// Sets default values
ACameraActorBase::ACameraActorBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ACameraActorBase::BeginPlay()
{
	Super::BeginPlay();
	ArmComponent = FindComponentByClass<USpringArmComponent>();
	CamComponent = FindComponentByClass<UCameraComponent>();
}
// Called every frame
void ACameraActorBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

