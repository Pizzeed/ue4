#include "WeaponBase.h"
#include "PlayerBase.h"
#include "ProjectileBase.h"
#include "Kismet/KismetSystemLibrary.h"
#include "Components/SkeletalMeshComponent.h"
#include "Components/StaticMeshComponent.h"
#include "Components/SphereComponent.h"
#include "Components/AudioComponent.h"
#include "InventoryComponent.h" 

#include "StatusEffect.h"
#include "Particles/ParticleSystemComponent.h"

//const FLoadedAmmo FLoadedAmmo::None = FLoadedAmmo(FAmmo::None);

FWeaponInventoryItem::FWeaponInventoryItem()
{
	Type = EWeaponType::Melee;
	WeaponClass = AWeaponBase::StaticClass();
}

AWeaponBase::AWeaponBase()
{
	PrimaryActorTick.bCanEverTick = true;

	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("Scene"));
	
	SkeletalMesh = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("Skeletal Mesh"));
	SkeletalMesh->SetGenerateOverlapEvents(false);
	SkeletalMesh->SetCollisionProfileName(TEXT("NoCollision"));
	SkeletalMesh->SetupAttachment(RootComponent);

	Mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Static Mesh "));
	Mesh->SetGenerateOverlapEvents(false);
	Mesh->SetCollisionProfileName(TEXT("NoCollision"));
	Mesh->SetupAttachment(RootComponent);

	WeaponFX =CreateDefaultSubobject<UParticleSystemComponent>(TEXT("Shot FX"));
	WeaponFX->SetupAttachment(RootComponent);
	WeaponFX->bAutoActivate = false;

	Audio = CreateDefaultSubobject<UAudioComponent>(TEXT("Shot Sound"));
	Audio->SetupAttachment(RootComponent);
	Audio->bAutoActivate = false;
}

void AWeaponBase::BeginPlay()
{
	Super::BeginPlay();
	
}

void AWeaponBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (IsAttacking && CheckAttackAvailable())
	{
		Attack();
	}
	else
	{
		AttackCooldown = AttackCooldown < 0 ? AttackCooldown = 0 : AttackCooldown -= DeltaTime;
	}

}

void AWeaponBase::Attack()
{

}

bool AWeaponBase::CheckAttackAvailable()
{
	return false;
}
