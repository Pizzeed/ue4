#include "Room.h"
#include "RoomSpawner.h"
#include "Components/SceneComponent.h"
#include "Components/SpotlightComponent.h"

ARoom::ARoom(const FObjectInitializer& OI) : Super(OI)
{
	PrimaryActorTick.bCanEverTick = true;
	RootComponent = OI.CreateDefaultSubobject<USceneComponent>(this, "Root");
	MainFloorPlatform = CreateDefaultSubobject<UStaticMeshComponent>("Floor");
	LightSource = CreateDefaultSubobject<USpotLightComponent>("Light");
	LightSource->SetupAttachment(RootComponent);
	LightSource->SetRelativeLocation(FVector(0,0,750));
	LightSource->SetWorldRotation(FRotator(-90, 0, 0));
	LightSource->SetIntensity(1000000);
	LightSource->SetLightColor(FColor::FromHex("FF9721FF"));
	MainFloorPlatform->AttachTo(RootComponent);
}

void ARoom::BeginPlay()
{
	Super::BeginPlay();
	FVector minpoint;
	FVector maxpoint;
	if (MainFloorPlatform)
	{	
		MainFloorPlatform->GetLocalBounds(minpoint, maxpoint);
		RoomSpawnerOffset = abs((minpoint * MainFloorPlatform->GetComponentScale()).X)*2;

		if (IsTopDoor)
		{
			ARoomSpawner* spawner = GetWorld()->SpawnActor<ARoomSpawner>(RoomSpawnerClass,FTransform(FVector( this->GetActorLocation().X + RoomSpawnerOffset, this->GetActorLocation().Y, this->GetActorLocation().Z)));
			if (spawner) spawner->thisDirection = Direction::Bottom;
		}	
		if (IsBottomDoor)
		{
			ARoomSpawner* spawner = GetWorld()->SpawnActor<ARoomSpawner>(RoomSpawnerClass,FTransform(FVector( this->GetActorLocation().X - RoomSpawnerOffset, this->GetActorLocation().Y, this->GetActorLocation().Z)));
			if (spawner) spawner->thisDirection = Direction::Top;
		}	
		if (IsLeftDoor)
		{
			ARoomSpawner* spawner = GetWorld()->SpawnActor<ARoomSpawner>(RoomSpawnerClass,FTransform(FVector( this->GetActorLocation().X, this->GetActorLocation().Y - RoomSpawnerOffset, this->GetActorLocation().Z)));
			if (spawner) spawner->thisDirection = Direction::Right;
		}	
		if (IsRightDoor)
		{
			ARoomSpawner* spawner = GetWorld()->SpawnActor<ARoomSpawner>(RoomSpawnerClass,FTransform(FVector( this->GetActorLocation().X, this->GetActorLocation().Y + RoomSpawnerOffset, this->GetActorLocation().Z)));
			if(spawner) spawner->thisDirection = Direction::Left;
		}
	}
}

void ARoom::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

