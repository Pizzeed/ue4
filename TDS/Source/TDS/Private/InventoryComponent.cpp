#include "InventoryComponent.h"

UInventoryComponent::UInventoryComponent()
{
	PrimaryComponentTick.bCanEverTick = true;
}

bool UInventoryComponent::EqualEqual_FUsableItemFUsableItem(FUsableItem A, const FUsableItem B)
{
	return
		A.Stack == B.Stack &&
		A.bStackable == B.bStackable &&
		A.ItemQuality == B.ItemQuality &&
		A.ItemName == B.ItemName;
}

void UInventoryComponent::BeginPlay()
{
	Super::BeginPlay();
}

void UInventoryComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
}

int UInventoryComponent::GetAmmoAmountOfType(EAmmoType type)
{
	int result = 0;
	for (FLoadedAmmo item : AmmoOwned)
	{
		if (item.Info.Type == type && item.Amount > 0)
			result += item.Amount;
	}
	return result;
}

int UInventoryComponent::GetAmmoAmountOfTypeArr(TArray<EAmmoType> type)
{
	int result = 0;
	/*for (EAmmoType t : type)
	{
		for (FAmmo item : AmmoOwned)
		{
			if (item.Type == t && item.Quantity > 0)
				result += item.Quantity;
		}
	}*/
	return result;
}
