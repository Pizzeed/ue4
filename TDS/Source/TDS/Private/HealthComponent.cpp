#include "HealthComponent.h"

UHealthComponent::UHealthComponent()
{
	PrimaryComponentTick.bCanEverTick = true;

}

void UHealthComponent::BeginPlay()
{
	Super::BeginPlay();

}

void UHealthComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

}

void UHealthComponent::Damage(int Amount, AActor* sender)
{
	CurrentHealth -= (int)(Amount * DamageCoefficient);
	OnDamageTaken.Broadcast(GetOwner(), sender, Amount);
}

void UHealthComponent::Die(AActor* Killer)
{
	OnDead.Broadcast(GetOwner(), Killer);
}

void UHealthComponent::Heal(int Amount, AActor* sender)
{
	CurrentHealth = CurrentHealth + Amount >= MaxHealth ? MaxHealth : CurrentHealth + (int)(Amount * HealCoefficient);
	OnHealthGained.Broadcast(GetOwner(), sender, Amount);
}

