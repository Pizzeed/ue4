#include "RoomSpawner.h"
#include "RoomCollection.h"
#include "Room.h"
#include "TDSGameModeBase.h"


ARoomSpawner::ARoomSpawner()
{
	BoxComponent = CreateDefaultSubobject<UBoxComponent>("BoxCollision");
	RootComponent = BoxComponent;
	BoxComponent->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	BoxComponent->SetCollisionResponseToAllChannels(ECR_Overlap);
	BoxComponent->OnComponentBeginOverlap.AddDynamic(this, &ARoomSpawner::OnActorBeginOverlapHandler);
}

void ARoomSpawner::BeginPlay()
{
	Super::BeginPlay();
	if (!bIsRoomSpawned)
	{
		FTimerHandle UnusedHandle;
		GetWorldTimerManager().SetTimer(UnusedHandle, this, &ARoomSpawner::SpawnRoom, 0.3f, false);
	}
}

void ARoomSpawner::SpawnRoom()
{
	TArray<TSubclassOf<ARoom>> chosenRooms;
	ATDSGameModeBase* currentGM = Cast<ATDSGameModeBase>(GetWorld()->GetAuthGameMode());
	if (IsValid(currentGM))
	{
		if (IsStarter)
		{
			RoomClass = StarterRoomClass;
		}
		else
		{
			switch (thisDirection)
			{
				case Direction::Top:
				{
					if (currentGM->CurrentRooms < currentGM->MaxRooms)
					{
						chosenRooms = currentGM->CurrentCollection.TopDoorRooms;
					}							
					else						
					{							
						chosenRooms = currentGM->CurrentCollection.TopDoorDeadEndRooms;
					}
				}
				break;
				case Direction::Right:
				{
					if (currentGM->CurrentRooms < currentGM->MaxRooms)
					{
						chosenRooms = currentGM->CurrentCollection.RightDoorRooms;
					}							 
					else						 
					{							 
						chosenRooms = currentGM->CurrentCollection.RightDoorDeadEndRooms;
					}							 
				}								 
				break;							 
				case Direction::Bottom:			 
				{								 
					if (currentGM->CurrentRooms < currentGM->MaxRooms)
					{							
						chosenRooms = currentGM->CurrentCollection.BottomDoorRooms;
					}							
					else						
					{							
						chosenRooms = currentGM->CurrentCollection.BottomDoorDeadEndRooms;
					}
				}
				break;
				case Direction::Left:
				{
					if (currentGM->CurrentRooms < currentGM->MaxRooms)
					{
						chosenRooms = currentGM->CurrentCollection.LeftDoorRooms;
					}
					else
					{
						chosenRooms = currentGM->CurrentCollection.LeftDoorDeadEndRooms;
					}
				}
				break;
			}
			int rnd = FMath::RandRange(0, chosenRooms.Num() - 1);
			RoomClass = chosenRooms[rnd];
		}
	}

	GetWorld()->SpawnActor<ARoom>(RoomClass, FTransform(this->GetActorLocation()));
	bIsRoomSpawned = true;
	currentGM->CurrentRooms++;
}

void ARoomSpawner::OnActorBeginOverlapHandler(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (IsValid(Cast<ARoomSpawner>(OtherActor)) && !bIsRoomSpawned)
	{
		if (Cast<ARoomSpawner>(OtherActor)->bIsRoomSpawned)
		{
			this->Destroy();
		}
		else if (thisDirection > Cast<ARoomSpawner>(OtherActor)->thisDirection)
		{
			this->Destroy();
		}
	}
}

