#include "StatusEffect.h"

void AStatusEffect::ApplyEffect()
{
	OnStatusEffectApplied.Broadcast(this, ImpliedTo);
}

void AStatusEffect::BeginPlay()
{
	Super::BeginPlay();
}

void AStatusEffect::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}
