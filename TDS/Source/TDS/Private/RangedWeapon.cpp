#include "RangedWeapon.h"

#include "Components/ArrowComponent.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "Components/SphereComponent.h"
#include "DrawDebugHelpers.h"

#include "PlayerBase.h"
#include "ProjectileBase.h"
#include "InventoryComponent.h" 


ARangedWeapon::ARangedWeapon()
{
	GunPoint = CreateDefaultSubobject<USceneComponent>(TEXT("GunPoint"));
	GunPoint->SetupAttachment(RootComponent);
	GunPoint->SetRelativeRotation(FRotator(0, 90, 0));

	Arrow = CreateDefaultSubobject<UArrowComponent>(TEXT("ShootLocation"));
	Arrow->SetupAttachment(RootComponent);
	
	//AmmoIn = FLoadedAmmo::None;
}

void ARangedWeapon::BeginPlay()
{
	Super::BeginPlay();
}

void ARangedWeapon::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void ARangedWeapon::Reload()
{
	if (OwnerChar)
	{
		if (OwnerChar->HasAmmo(AmmoIn.Info))
		{
			int hasAmmo = OwnerChar->Inventory->AmmoOwned[OwnerChar->Inventory->AmmoOwned.Find(FLoadedAmmo(AmmoIn.Info))].Amount;
			if (hasAmmo <= MaxAmmo - AmmoIn.Amount)
			{
				AmmoIn.Amount += hasAmmo;
				OwnerChar->Inventory->AmmoOwned.Remove(AmmoIn.Info);
			}
			else
			{
				OwnerChar->Inventory->AmmoOwned[OwnerChar->Inventory->AmmoOwned.Find(AmmoIn.Info)].Amount -= MaxAmmo - AmmoIn.Amount;
				AmmoIn.Amount = MaxAmmo;
			}
		}
		else if (OwnerChar->HasAmmo(AllowedAmmo))
		{
			AmmoIn.Info = OwnerChar->FindAmmo(AllowedAmmo);
			if (OwnerChar->Inventory->AmmoOwned[OwnerChar->Inventory->AmmoOwned.Find(AmmoIn.Info)].Amount > MaxAmmo - AmmoIn.Amount)
			{
				OwnerChar->Inventory->AmmoOwned[OwnerChar->Inventory->AmmoOwned.Find(AmmoIn.Info)].Amount -= MaxAmmo - AmmoIn.Amount;
				AmmoIn.Amount = MaxAmmo;
			}
			else
			{
				AmmoIn.Amount += OwnerChar->Inventory->AmmoOwned[OwnerChar->Inventory->AmmoOwned.Find(AmmoIn.Info)].Amount;
				OwnerChar->Inventory->AmmoOwned.Remove(AmmoIn);
			}
		}
	}
}

void ARangedWeapon::Attack()
{
	if (IsMultishot)
		Shoot(AmmoIn.Amount >= ShotCount || !MultishotUseAmmoForEachShot ? ShotCount : AmmoIn.Amount);
	else
		Shoot();
}

bool ARangedWeapon::CheckAttackAvailable()
{
	return AttackCooldown <= 0 && AmmoIn.Amount > 0 && !OwnerChar->bIsSprinting;
}

void ARangedWeapon::Shoot(int amount = 1)
{
	if (ShotType == ERangedWeaponType::Projectile)
	{
		if (amount == 1)
		{
			if (Arrow)
			{
				FVector SpawnLocation = GunPoint->GetComponentLocation();
				FRotator SpawnRotation = Arrow->GetComponentRotation();

				if (ProjectileClass && AmmoIn.Amount > 0)
				{
					FActorSpawnParameters SpawnParams;
					SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
					SpawnParams.Owner = GetOwner();
					SpawnParams.Instigator = GetInstigator();

					if (AProjectileBase* newProjectile = Cast<AProjectileBase>(GetWorld()->SpawnActor(ProjectileClass, &SpawnLocation, &SpawnRotation, SpawnParams)))
					{
						newProjectile->AmmoInfo = AmmoIn.Info;
						FVector newVel = (Arrow->GetRelativeLocation() - GunPoint->GetRelativeLocation()) * ProjectileStartSpeed;
						newProjectile->ProjectileMovement->SetVelocityInLocalSpace(newVel);
						//newProjectile->ApplySettings();
						//newProjectile->InitialLifeSpan = WeaponInfo.Projectile.LifeSpan;
					}
					AmmoIn.Amount--;
					AttackCooldown = 1/RateOfFire;
				}
			}
		}
		else if (amount > 1)
		{
			for (int i = -(amount / 2); i <= amount / 2; i++)
			{

				if (Arrow)
				{
					FVector SpawnLocation = GunPoint->GetComponentLocation();
					FRotator SpawnRotation = GunPoint->GetComponentRotation();

					if (ProjectileClass && AmmoIn.Amount > 0)
					{
						FActorSpawnParameters SpawnParams;
						SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
						SpawnParams.Owner = GetOwner();
						SpawnParams.Instigator = GetInstigator();

						if (AProjectileBase* newProjectile = Cast<AProjectileBase>(GetWorld()->SpawnActor(ProjectileClass, &SpawnLocation, &SpawnRotation, SpawnParams)))
						{
							newProjectile->AmmoInfo = AmmoIn.Info;
							if (MultishotType == EMultishotType::Radial)
							{
								float minangle = (MultishotRadialAngleDeg / ShotCount - 1) * (PI / 180.f);
								float minoffset = (sin(minangle / 2.f) / sin((180.f - minangle) / 2.f));
								float VectOffset = minoffset * i;
								FVector newVel = FVector(Arrow->GetRelativeLocation().X, Arrow->GetRelativeLocation().Y + VectOffset, Arrow->GetRelativeLocation().Z) - GunPoint->GetRelativeLocation().Normalize() * ProjectileStartSpeed;
								newProjectile->CollisionSphere->AddForce(newVel);

								//newProjectile->ProjectileMovement->SetVelocityInLocalSpace((FVector(Arrow->GetRelativeLocation().X+VectOffset, Arrow->GetRelativeLocation().Y , Arrow->GetRelativeLocation().Z) - GunPoint->GetRelativeLocation()).GetSafeNormal() * WeaponInfo.ProjectileStartSpeed);
							}
							//	newProjectile->ApplySettings();
							//	newProjectile->InitialLifeSpan = WeaponInfo.Projectile.LifeSpan;
						}
					}
				}
				AttackCooldown = 1/RateOfFire;
			}
			if (MultishotUseAmmoForEachShot)
				AmmoIn.Amount -= amount;
			else
				AmmoIn.Amount--;
		}
	}
	else if (ShotType == ERangedWeaponType::Hitscan)
	{
		if (amount == 1)
		{
			FHitResult OutHit;
			FVector Start = GunPoint->GetComponentLocation();
			FVector End = GunPoint->GetComponentLocation() + GunPoint->GetForwardVector() * RangeOfFire;
			FCollisionQueryParams CollisionParams;
			//GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, End.ToString());
			DrawDebugLine(GetWorld(), Start, End, FColor::Red, false, .2, 0, 5);

			if (GetWorld()->LineTraceSingleByChannel(OutHit, Start, End, ECC_WorldDynamic, CollisionParams))
			{
				GEngine->AddOnScreenDebugMessage(-1, 1.f, FColor::Green, "HIT!");
			}
		}
		else if (amount > 1)
		{
			for (int i = -(amount / 2); i <= amount / 2; i++)
			{
				GunPoint->SetRelativeRotation(FRotator(0.f, 90.f, 0.f));
				GunPoint->SetRelativeRotation(FRotator(0.f, GunPoint->GetRelativeRotation().Yaw + (MultishotRadialAngleDeg / (ShotCount - 1)) * i, 0.f));
				FHitResult OutHit;
				FVector Start = GunPoint->GetComponentLocation();
				FVector End = GunPoint->GetComponentLocation() + GunPoint->GetForwardVector() * RangeOfFire;
				FCollisionQueryParams CollisionParams;
				//GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, GunPoint->GetRelativeRotation().ToString());
				DrawDebugLine(GetWorld(), Start, End, FColor::Red, false, .2, 0, 5);

				if (GetWorld()->LineTraceSingleByChannel(OutHit, Start, End, ECC_WorldDynamic, CollisionParams))
				{
					GEngine->AddOnScreenDebugMessage(-1, 1.f, FColor::Green, "HIT!");
				}
			}
		}
		AttackCooldown = 1/RateOfFire;
		if (MultishotUseAmmoForEachShot)
			AmmoIn.Amount -= amount;
		else
			AmmoIn.Amount--;
	}
}
