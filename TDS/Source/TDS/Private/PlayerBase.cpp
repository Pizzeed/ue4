#include "PlayerBase.h"
#include "WeaponBase.h"
#include "InventoryComponent.h"  
#include "CameraActorBase.h"
#include "PickupItem.h"
#include "RangedWeapon.h"
#include "Types.h"
#include "Components/CapsuleComponent.h"

#include "GameFramework/SpringArmComponent.h"

APlayerBase::APlayerBase()
{
	PrimaryActorTick.bCanEverTick = true;
	Inventory = CreateDefaultSubobject<UInventoryComponent>("Inventory");
}

void APlayerBase::BeginPlay()
{
	Super::BeginPlay();
	CurrentStamina = MaxStamina;
}

void APlayerBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	
	MovementVector.Normalize(1.f);
	AddActorWorldOffset(MovementVector * CurrentMovementSpeed * DeltaTime * 100, true);
	if(Cam)
	Cam->SetActorLocation(FVector(GetActorLocation().X-200, GetActorLocation().Y, Cam->GetActorLocation().Z));
	if (bIsSprinting)
	{
		if (CurrentStamina > 0)
		{
			CurrentStamina--;
		}
		else 
		{
			bIsSprinting = false;
			CurrentMovementSpeed = DefaultMovementSpeed;
		}
	}
	else if(CurrentStamina <= MaxStamina)
	{
		CurrentStamina+= 0.5;
	}
}


void APlayerBase::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
	PlayerInputComponent->BindAxis("Horizontal", this, &APlayerBase::HorizontalInput);
	PlayerInputComponent->BindAxis("Vertical", this, &APlayerBase::VerticalInput);
	PlayerInputComponent->BindAxis("CameraScale", this, &APlayerBase::CameraScale);
	PlayerInputComponent->BindAxis("WeaponSwitch", this, &APlayerBase::SwitchWeaponHandler);
	PlayerInputComponent->BindAction("Reload", IE_Pressed, this, &APlayerBase::ReloadStart);
	PlayerInputComponent->BindAction("PickUp", IE_Pressed, this, &APlayerBase::OnPickUp);
	FInputActionBinding RunPressedAB("Run", IE_Pressed);
	FInputActionBinding RunReleasedAB("Run", IE_Released);
	RunPressedAB.ActionDelegate.GetDelegateForManualSet().BindLambda([this]()
	{
		if (CurrentStamina > 0)
		{
			CurrentMovementSpeed *= RunModifier;
			bIsSprinting = true;
			bIsAttacking = false;
		}
	});
	RunReleasedAB.ActionDelegate.GetDelegateForManualSet().BindLambda([this]()
	{
		CurrentMovementSpeed = DefaultMovementSpeed;
		bIsSprinting = false;
	});
	InputComponent->AddActionBinding(RunPressedAB);
	InputComponent->AddActionBinding(RunReleasedAB);
}

void APlayerBase::HorizontalInput(float value)
{
	MovementVector.Y = value;
}

void APlayerBase::VerticalInput(float value)
{
	MovementVector.X = value;
}

void APlayerBase::CameraScale(float value)
{
	if (IsValid(Cam))
	{
		if (!(Cam->ArmComponent->GetRelativeLocation().X + value < -MaxCameraDistance || Cam->ArmComponent->GetRelativeLocation().X + value > 0))
		{
			Cam->ArmComponent->AddRelativeLocation(FVector(value, 0, 0));
		}
	}
}

void APlayerBase::SwitchWeaponHandler(float value)
{
	if (Inventory && Inventory->Weapons.Num() > 1 && !bIsReloading && !bIsAttacking)
	{
		FWeaponInventoryItem saveWeapon = Cast<ARangedWeapon>(CurrentWeapon) ? FWeaponInventoryItem(CurrentWeapon->GetClass(), Cast<ARangedWeapon>(CurrentWeapon)->AmmoIn) : FWeaponInventoryItem(CurrentWeapon->GetClass());
		Inventory->Weapons[Inventory->EquippedWeapon] = saveWeapon;
		FWeaponInventoryItem switchTo;
		if (value > 0)
		{
			if (Inventory->EquippedWeapon + 1 == Inventory->Weapons.Num())
			{
				switchTo = Inventory->Weapons[0];
				Inventory->EquippedWeapon = 0;
			}
			else
			{
				Inventory->EquippedWeapon++;
				switchTo = Inventory->Weapons[Inventory->EquippedWeapon];
			}
		}
		else if (value < 0)
		{
			if (Inventory->EquippedWeapon == 0)
			{
				switchTo = Inventory->Weapons.Last();
				Inventory->EquippedWeapon = Inventory->Weapons.Num()-1;
			}
			else
			{
				Inventory->EquippedWeapon--;
				switchTo = Inventory->Weapons[Inventory->EquippedWeapon];
			}
		}

		if (value != 0)
		{
			CurrentWeapon->Destroy();

			CurrentWeapon = GetWorld()->SpawnActor<AWeaponBase>(switchTo.WeaponClass, GetActorLocation(), GetActorRotation());
			CurrentWeapon->OwnerChar = this;
			if(Cast<ARangedWeapon>(CurrentWeapon))
			{
				Cast<ARangedWeapon>(CurrentWeapon)->AmmoIn = switchTo.LoadedAmmo;
			}
			CurrentWeapon->AttachToComponent(GetSkeletalMesh(), FAttachmentTransformRules::SnapToTargetNotIncludingScale, weaponSocketName);
		}
	}
	UpdateInventory();
}

bool APlayerBase::HasAmmo(FAmmo type)
{
	/*if (Inventory)
	{
		for (FAmmo item : Inventory->AmmoOwned)
		{
			if (item == type && item.Quantity > 0)
			{
				return true;
			}
		}
		return false;
	}
	else*/ return false;
}

bool APlayerBase::HasAmmo(EAmmoType type)
{
	/*for (FAmmo item : Inventory->AmmoOwned)
	{
		if (item.Type == type && item.Quantity > 0)
			return true;
	}*/
	return false;
}

bool APlayerBase::HasAmmo(TArray<EAmmoType> types)
{
	/*for (EAmmoType t : types)
	{
		for (FAmmo item : Inventory->AmmoOwned)
		{
			if (item.Type == t && item.Quantity > 0)
				return true;
		}
	}*/
	return false;
}

FAmmo APlayerBase::FindAmmo(TArray<EAmmoType> types)
{
	/*for (EAmmoType t : types)
	{
		for (FAmmo item : Inventory->AmmoOwned)
		{
			if (item.Type == t && item.Quantity > 0)
				return item;
		}
	}*/
	return FAmmo();
}

FAmmo APlayerBase::FindAmmo(EAmmoType type)
{
	/*for (FAmmo item : Inventory->AmmoOwned)
	{
		if (item.Type == type && item.Quantity > 0)
			return item;
	}*/
	return FAmmo();
}

void APlayerBase::ReloadStart()
{
	/*if (!bIsReloading && (HasAmmo(this->CurrentWeapon->WeaponInfo.LoadedAmmo) || HasAmmo(CurrentWeapon->WeaponInfo.AllowedAmmo)))
	{
		bIsReloading = true;
	}*/
}



void APlayerBase::ReloadFinish()
{
	bIsReloading = false;
	/*if (CurrentWeapon) CurrentWeapon->Reload();*/
	UpdateInventory();
}

void APlayerBase::OnPickUp()
{
	//UCapsuleComponent* caps = FindComponentByClass<UCapsuleComponent>();
	//TArray<AActor*> arr;
	//caps->GetOverlappingActors(arr, TSubclassOf<APickupItem>());
	//if (arr.Num() > 0)
	//{
	//	APickupItem* item = Cast<APickupItem>(arr[0]);
	//	switch (item->PickupType)
	//	{
	//		case EPickupType::Ammo: 
	//		{
	//			if (item->AmmoInfo.bStackable)
	//			{
	//				bool flag = false;
	//				for (int i = 0; i < Inventory->AmmoOwned.Num(); i++)
	//				{
	//					if (Inventory->AmmoOwned[i] == item->AmmoInfo)
	//					{
	//						Inventory->AmmoOwned[i].Quantity += item->AmmoInfo.Quantity;
	//						item->Destroy();
	//						flag = true;
	//						break;
	//					}
	//				}
	//				if (!flag && Inventory->AmmoOwned.Num() < Inventory->AmmoSlots)
	//				{
	//					Inventory->AmmoOwned.Add(item->AmmoInfo);
	//					item->Destroy();
	//				}
	//			}
	//			else if (Inventory->AmmoOwned.Num() < Inventory->AmmoSlots)
	//			{
	//				Inventory->AmmoOwned.Add(item->AmmoInfo);
	//				item->Destroy();
	//			}
	//		} 
	//		break;
	//		case EPickupType::Usable: 
	//		{
	//			if (Inventory->UsableItems.Num() == 0 && Inventory->UsableItemSlots > 0)
	//			{
	//				Inventory->UsableItems.Add(item->UsableItemInfo);
	//				item->Destroy();
	//			}
	//			else if (item->UsableItemInfo.bStackable)
	//			{
	//				bool flag;
	//				for (int i = 0; i < Inventory->UsableItems.Num(); i++)
	//				{
	//					if (Inventory->UsableItems[i] == item->UsableItemInfo && Inventory->UsableItems[i].Quantity != Inventory->UsableItems[i].Stack)
	//					{
	//						flag = true;
	//						if (Inventory->UsableItems[i].Stack - Inventory->UsableItems[i].Quantity >= item->UsableItemInfo.Quantity)
	//						{
	//							Inventory->UsableItems[i].Quantity += item->UsableItemInfo.Quantity;
	//							item->Destroy();
	//						}
	//						else
	//						{
	//							Inventory->UsableItems[i].Quantity = Inventory->UsableItems[i].Stack;
	//							item->UsableItemInfo.Quantity -= Inventory->UsableItems[i].Stack - Inventory->UsableItems[i].Quantity;
	//						}
	//						break;
	//					}
	//				}
	//				if (Inventory->UsableItems.Num() < Inventory->AmmoSlots && !flag)
	//				{
	//					Inventory->UsableItems.Add(item->UsableItemInfo);
	//					item->Destroy();
	//				}
	//			}
	//		} 
	//		break;
	//		case EPickupType::Weapon: 
	//		{
	//			/*if (Inventory->Weapons.Num() < Inventory->WeaponSlots)
	//			{
	//				if (Inventory->Weapons.Num() == 0)
	//				{
	//					CurrentWeapon = GetWorld()->SpawnActor<AWeaponBase>(item->WeaponInfo.WeaponClass, GetActorLocation(), GetActorRotation());
	//					if (CurrentWeapon)
	//					{
	//						CurrentWeapon->WeaponInfo = item->WeaponInfo;
	//						CurrentWeapon->AttachToComponent(GetMesh(), FAttachmentTransformRules::SnapToTargetNotIncludingScale, weaponSocketName);
	//						CurrentWeapon->OwnerChar = this; 
	//						CurrentWeapon->WeaponInfo.CurrentAmmo = 0;
	//						Inventory->EquippedWeapon = 0;
	//					}
	//				}
	//				Inventory->Weapons.Add(item->WeaponInfo);
	//				item->Destroy();
	//			}*/
	//		} 
	//		break;

	//	}
	//	
	//}
	UpdateInventory();
}

void APlayerBase::SwitchAmmo(FAmmo switchTo)
{
	/*for(EAmmoType item : CurrentWeapon->WeaponInfo.AllowedAmmo)
		if (switchTo.Type == item)
		{
			CurrentWeapon->WeaponInfo.LoadedAmmo = switchTo;
			break;
		}*/
}
