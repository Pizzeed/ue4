#include "PickupItem.h"
#include "Types.h"

APickupItem::APickupItem()
{
	PrimaryActorTick.bCanEverTick = true;

}

void APickupItem::BeginPlay()
{
	Super::BeginPlay();
	
}

void APickupItem::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (bRotate)
	{
		AddActorWorldRotation(FRotator(0,1,0), false);
	}

}

void APickupItem::UseItem()
{
}

void APickupItem::PickUp()
{
}

